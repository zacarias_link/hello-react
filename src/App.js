import logo from "./logo.svg";
import "./App.css";
import Text from "./components/Text/Text.jsx";
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>
          Hello,{" "}
          <Text
            color="gray"
            value="diego de sena oliveira - kenzie academy brasil"
          />
        </h1>
      </header>
    </div>
  );
}

export default App;
