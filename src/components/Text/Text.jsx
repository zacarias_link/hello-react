import { Component } from "react";

class Text extends Component {
  render() {
    return (
      <h1 style={{ color: this.props.color }}>Hello, {this.props.value}</h1>
    );
  }
}

export default Text;
